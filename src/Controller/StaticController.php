<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StaticController extends AbstractController
{
    #[Route('/mentions-legales', name: 'mention_legales')]
    public function mentionLegales(): Response
    {
        return $this->render('static/mention-legales.html.twig');
    }
}
